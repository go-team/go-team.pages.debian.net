# pkg-go team website

This repository contains the pkg-go team website. Build it with ``make build``.

It is published with Gitlab Pages at https://go-team.pages.debian.net/

To save resources on the Gitlab runners, as suggested by the [Salsa documentation](https://wiki.debian.org/Salsa/Doc#Web_page_hosting), we also commit the built website in the "build" folder.
