Debian Go Packaging
===================
:toc:

After reading this document, you should be able to produce Debian packages for Go software.

== General notes

=== Team Maintenance

All Go packages are team-maintained in the pkg-go team. This has multiple advantages:

 . There are no single points of failure, if one maintainer cannot continue
   maintaining a package, another maintainer can easily help out or take over.
 . All packages can share the same technical standards and workflows.
 . When working within a team anyway, it is much easier to get help on any
   technical issue.

A package maintained within the team should have the name of the team either in
the `Maintainer` field or in the `Uploaders` field. We use tracker.debian.org's
team functionality and spell the team like this:
`Debian Go Packaging Team <team+pkg-go@tracker.debian.org>`. This enables the
team to have an overview of its packages on the DDPO website and ensures that
all packages maintained by the team are automatically added to the Debian Package
Tracker team.

Putting the team in `Maintainers` is a strong statement that fully
collaborative maintenance is preferred. Anyone can commit to the git repository
and upload as needed. A courtesy email to `Uploaders` can be nice but not
required.

If for some reason, a package needs tighter control by its original
maintainers, putting the team in `Uploaders` is a weak statement of
collaboration. Help in maintaining the package is appreciated, commits to the
git repository are freely welcomed, but before uploading, please contact the
`Maintainer` for the green light.

//////////////////////////////////////////////////////////////////////////
// TODO: add an example link
//////////////////////////////////////////////////////////////////////////

=== Packaging in git

All Go packages are maintained in git and must be buildable `gbp build-package` using
https://honk.sigxcpu.org/projects/git-buildpackage/manual-html/[git-buildpackage].

We use https://salsa.debian.org/go-team/packages/[Salsa] to store the git repositories.

=== Git setup for automatically authenticating

When using git-buildpackage’s vcsgit feature (e.g. `gbp clone
vcsgit:golang-text`), the following configuration results in git automatically
rewriting URLs to be authenticated:

--------------------------------------------------------------------------------
git config --global url."git.debian.org:/git/".insteadOf "https://anonscm.debian.org/git/"
git config --global url."git@salsa.debian.org:".insteadOf "https://salsa.debian.org/"
--------------------------------------------------------------------------------

=== Using dh-make-golang

Instead of copying the debian/ directory from some random other package, please
use
https://people.debian.org/~stapelberg/2015/07/27/dh-make-golang.html[dh-make-golang]
when you start packaging a new Go library/program.

Also `dh-make-golang` can create Salsa projects, and configure the link:ci.html[CI]
automatically. Guest accounts on Salsa can't create projects in
go-team group, they can only do it with this approach. Please run
`dh-make-golang create-salsa-project` to see the usage.

=== Version numbers

Many Go libraries (and also some actual Go programs) don’t have version numbers
in the traditional sense, but live in a version control repository of some
kind.

In case your upstream does not use version numbers, the Debian package version
will look like this:

0.0~git20130606.b00ec39-1

* The 0.0 in the beginning is used to allow upstream to adopt version numbers at
  any point in time and also to make it clear that no traditional version number
  is used for this package.
* The second part is the version control system, e.g.  git, hg, svn.
* Afterwards, a date follows in the format YYYYMMDD.
* After the dot, the version control system revision follows, to make it clear
  which commit was packaged, as many repositories have multiple commits on a
  given day.
* The last part after the dash is the Debian version number.

In case you make more than one snapshot per day, you can append a snapshot
number after the date, e.g. 0.0~git20130606.2.b00ec39-1. This should rarely be
necessary.

=== changelog: UNRELEASED

During the time when you still work on a package, i.e. before it is ready to
upload, please put `UNRELEASED` into the distribution field in
`debian/changelog` (`dch -v <debian_version>` will do it automatically). When
the package is ready for uploading, change it to `unstable` (`dch -r`).

If you change something that has to be noted in debian/changelog, just add a
line to the current entry (dch -a). The [firstname lastname] markers added by
dch are okay to give credit to non-upload-permitted contributors (also for the
initial changelog entry).

Important NOTES to other group members may be placed at the top of the current
changelog entry of packages that are not yet ready for upload (e.g. why a
package is still `UNRELEASED`, etc.).

== Binary-only packages

A binary-only package is a package that contains a program written in Go, but
no source code. An example is `docker`, which is written in Go, but does not
offer an API (thus no source code).

=== Naming Conventions

The source package should be named like the upstream project, i.e. `docker`,
you do NOT need to call it `golang-docker`.

Similarly, the resulting binary package(s) should NOT contain the `golang-`
prefix.

=== Use dh-golang

Install `dh-golang` from Debian unstable so that you are using the newest
version. The buildds are using the unstable version, too, so this is important.

`dh-golang` comes with an example `debian/rules` file:
https://salsa.debian.org/go-team/packages/dh-golang/-/blob/debian/sid/example/rules

You will need to change the value of `XS-Go-Import-Path` in `debian/control`
to correspond to your program’s upstream package name. This is usually what you
would `go get` when installing it manually. `dh-golang` needs that information
so that it can run `go install`.

`dh-golang` sets up a build environment that contains all the libraries that
are available in `/usr/share/gocode/src`, so you need to add Build-Depends to
your package. As an example, Debian Code Search depends on
`golang-github-lib-pq-dev` and others:
https://github.com/Debian/dcs/blob/master/debian/control#L5

== Library (or binary + library) packages

Libraries written in Go are packaged for Debian with the only purpose of
*building* other Go programs for Debian. They are specifically not available
for users in their regular development workflow. For that, users should use `go
get`. The rationale behind this decision is:

 * By using `go get` you are forced to set up the environment variable
   `$GOPATH` which you need for other `go` commands (e.g. `go build`, `go
   test`, etc.).
 * Debian packages install files to system locations, which users cannot
   modify. That means that users would not be able to upgrade packages with the
   canonical `go get -u <package>`. Even worse, when using sudo to forcibly
   modify the system files, it still would not work since no VCS information is
   contained in the Debian packages.

=== Naming Conventions

We derive Debian package names from the import path by replacing all slashes
with dashes and using a canonical identifier instead of the hostname.

E.g., for `github.com/stapelberg/godebiancontrol` (which contains "go"
already), the resulting Debian package name is
`golang-github-stapelberg-godebiancontrol-dev`.

We use the -dev suffix to keep the namespace clean for shipping shared
libraries in the future, which would then be shipped as
golang-github-stapelberg-godebiancontrol.

In general, you should use the *import path* for deriving a name, not the
actual package name. Ideally, those are the same anyway.

Here are a couple examples:

[width="100%",cols="5,5",options="header"]
|=======
| Import path | Debian package name
| github.com/stapelberg/websocket | golang-github-stapelberg-websocket-dev
| code.google.com/p/go.net/websocket | golang-googlecode-go.net-websocket-dev
| golang.org/x/oauth2 | golang-golang-x-oauth2-dev
| google.golang.org/appengine | golang-google-appengine-dev
|=======

=== File locations

All files should be installed into `/usr/share/gocode/src/`, which corresponds
to `$GOPATH/src`. As an example, for `github.com/lib/pq`
(golang-github-lib-pq-dev), one of the files is
`/usr/share/gocode/src/github.com/lib/pq/buf.go`.

See https://packages.debian.org/sid/all/golang-github-lib-pq-dev/filelist for a
full example file list.

=== Dependencies

Your library package, e.g. `golang-github-lib-pq-dev`, needs to have all the other Go
libraries it depends on in its Depends line. The dependencies need to be
available at build time to run the tests (if any) and at installation time so
that other packages can be built.

=== Upstream package moves

Occasionally, upstream packages might move from one code hosting provider to a
different one, as was the case with code.google.com being discontinued and many
projects moving to GitHub.

Such a move should be mentioned in `debian/changelog` and a compatibility
symbolic link should be installed using `debian/links` (see
https://salsa.debian.org/go-team/packages/golang-go.crypto/commit/4e0a285a0989331edb5ff580797ab3574197534d
for an example). Since the location of the package is also contained in the
Debian package’s name, it should be renamed (see
https://salsa.debian.org/go-team/packages/golang-go.net/commit/189085288e608ca2720b32d551227d330a561123
for an example).
